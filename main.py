# -*- coding: utf8 -*-

from apscheduler.schedulers.blocking import BlockingScheduler
import vk
import requests
import time
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as mb
from tkinter import messagebox

# Ирина Мягкова
# i.myagkova
# 181082443
# https://yandex.ru/

sched = BlockingScheduler()

name = 'name'
vk_id = 'vk_id.'
randid = 'randid'
link = 'link'

root = tk.Tk()
root.title('Парсер работоспособности')
root.geometry('640x400+400+300')

label_name = tk.Label(root, text='Введите ваши Имя и Фамилию в VK:\nПример:Ирина Мягкова')
label_name.place(x=50, y=20)
entry_name = ttk.Entry(root, width=30)
entry_name.place(x=400, y=20)
entry_name_wait = tk.Label(root, text="Ожидание...", width=10)
entry_name_wait.place(x=280, y=20)

label_vk_id = tk.Label(root, text='Введите ваш VK id:\nПример:i.miag')
label_vk_id.place(x=70, y=70)
entry_vk_id = ttk.Entry(root, width=30)
entry_vk_id.place(x=400, y=70)
entry_vk_id_wait = tk.Label(root, text="Ожидание...", width=10)
entry_vk_id_wait.place(x=280, y=70)

label_randid = tk.Label(root, text='Введите ваш rand VK id:\nПример:182072247')
label_randid.place(x=70, y=120)
entry_randid = ttk.Entry(root, width=30)
entry_randid.place(x=400, y=120)
entry_randid_wait = tk.Label(root, text="Ожидание...", width=10)
entry_randid_wait.place(x=280, y=120)

label_link = tk.Label(root, text='Введите адрес вашего сайта:\nПример:https://yandex.ru/')
label_link.place(x=70, y=170)
entry_link = ttk.Entry(root, width=30)
entry_link.place(x=400, y=170)
entry_link_wait = tk.Label(root, text="Ожидание...", width=10)
entry_link_wait.place(x=280, y=170)


def buttonCallback():
    global name
    global vk_id
    global randid
    global link
    entry_name_wait['text'] = ''
    entry_vk_id_wait['text'] = ''
    entry_randid_wait['text'] = ''
    entry_link_wait['text'] = ''
    entry_click = tk.Label(root, text="Программа работает", width=20)
    entry_click.place(x=280, y=250)

    name = entry_name.get()
    vk_id = entry_vk_id.get()
    randid = entry_randid.get()
    link = entry_link.get()


Button1 = ttk.Button(root, text='Ввести', command=buttonCallback)
Button1.place(x=510, y=220)

root.mainloop()

# put your token here
session = vk.Session(access_token='45bf976d0f7fd7f63d70df3f655bf869cd5aaa7c'
                                  'd0e7b28744279d53967118f0020be1fe34cb8bc0b9052')
vk_api = vk.API(session)

# pass your VK account here
admins = [
    {
        "name": f'"{name}"',
        "vk_id": f'"{vk_id}"',
        "randid": f'"{randid}"',
    },
]

m = 0


@sched.scheduled_job('cron', hour="*", minute="12,13,14")
def monitor():
    # put address to ping here
    request_result = requests.get(f"'{link}'")
    print(request_result.status_code)
    status_code = request_result.status_code
    # put good response here
    request_head = request_result.headers
    print(request_head['Date'])

    if request_result.status_code != 200:
        print('Error!')
        f = open('statistic.txt', 'a')
        f.write(request_head['Date'])
        f.write('\t')
        f.write('Error!!!\n')
        for admin in admins:
            # put message in case bad response here
            message = "Сайт не работает! Код ошибки -  " + str(status_code) + "\nПожалуйста, " \
                                                                             "свяжитесь с Ириной для проверки. " \
                                                                             "Если она долго не отвечает, " \
                                                                             "позвоните 89234408834"
            try:
                chat_id = admin["vk_id"]
                random_id = int(str(round(time.time()))+str(admin.get("randid")))
                vk_api.messages.send(
                        message=message,
                        domain=chat_id,
                        random_id=random_id,
                        v=5.103
                    )
                time.sleep(2)
            except:
                print("VK нет")
    else:
        print('Success!')
        f = open('statistic.txt', 'a')
        f.write(request_head['Date'])
        f.write('\t')
        f.write('Success!\n')


@sched.scheduled_job('cron', hour="*", minute='')
def numline():

    def statistic():
        success_status = 0
        error_status = 0
        global m
        n = 0
        with open('statistic.txt', 'r') as f:
            for n in range(m):
                f.readline()
            # в x будет m+1 строка
            x = f.readline()
            i = 0
            # собираем все данные за 1 час
            f1 = open('Data_set.csv', 'a')
            while i <= 2:
                week = f.read(3)
                # print(week)
                f1.write(week + ";")

                empty = f.read(2)

                date = f.read(2)
                # print(date)
                f1.write(date + ";")

                empty = f.read(1)

                month = f.read(3)
                # print(month)
                f1.write(month + ";")

                empty = f.read(1)

                year = f.read(4)
                # print(year)
                f1.write(year + ";")

                empty = f.read(1)

                hour = f.read(2)
                # print(hour)
                f1.write(hour + ";")

                empty = f.read(1)

                minutes = f.read(2)
                # print(minutes)
                f1.write(minutes + ";")

                empty = f.read(1)

                second = f.read(2)
                # print(second)
                f1.write(second + ";")

                empty = f.read(1)

                GMT = f.read(3)
                # print(GMT)
                f1.write(GMT + ";")

                empty = f.read(1)

                status = f.read(8)
                # print(status)
                f1.write(status + ";")

                if status == "Success!":
                    success_status += 1
                else:
                    error_status += 1

                empty = f.read(1)

                f1.write(str(success_status) + ";")
                f1.write(str(error_status))
                f1.write("\n")

                i += 1

    global m
    statistic()
    m += 12
    print(m)


if __name__ == '__main__':
    # monitor()
    sched.start()
